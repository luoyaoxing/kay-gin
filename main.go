package main

import (
	"github.com/gin-gonic/gin"
	"kay-gin/app/loadConfig"
	"kay-gin/app/logger"
	"kay-gin/app/middleware"
	"kay-gin/app/route"
	"log"
)

const (
	SERVERNAME = "kay-gin"
)

func main() {
	err := logger.InitLogger(SERVERNAME)
	if err != nil {
		log.Fatalf("serverName:%s InitLogger failed err:%s", SERVERNAME, err.Error())
	}

	err = loadConfig.LoadFile("kay.json")
	if err != nil {
		log.Fatalf("serverName:%s LoadFile failed err:%s", SERVERNAME, err.Error())
	}

	r := gin.Default()

	// 使用中间件
	r.Use(middleware.RequestLatencyPercent())

	// 加载路由分组
	route.LoadRouteGroup(r)

	// 项目根目录
	r.GET("/", index)

	r.Run()

	logger.Infof("serverName:%s shutdown", SERVERNAME)
}

func index(c *gin.Context) {
	c.String(200, "Hello, Kay-Hello")
}
