# 源镜像
FROM golang:latest

# 镜像描述
MAINTAINER 星kay "2539723916@qq.com"

# 设置golang环境变量
ENV GOPROXY https://goproxy.cn,direct
ENV GO111MODULE on

# 设置镜像工作目录
WORKDIR /go/cache

ADD go.mod .
ADD go.sum .

RUN go mod download

# 创建容器日志目录
RUN mkdir -p /go/log

RUN mkdir -p /go/config

# 复制目录到指定容器下
COPY app/config /go/config

WORKDIR /go/release

# 把当前文件拷贝进镜像
ADD . .

# 编译golng包
RUN GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w" -installsuffix cgo -o kay-gin main.go

CMD ["./kay-gin"]






