package controller

import (
	"github.com/gin-gonic/gin"
	"kay-gin/app/logic"
	"net/http"
)

func PullImage(c *gin.Context) {
	imageName := c.Query("imageName")
	if imageName == "" {
		c.JSON(http.StatusOK, gin.H{
			"code": 10001,
			"msg":  "请输入镜像",
		})
		return
	}

	err := logic.PullImage(imageName)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": 10001,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
	})
	return
}
