package logic

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
	"kay-gin/app/loadConfig"
	"kay-gin/app/logger"
)

func PullImage(imageName string) error {
	account := loadConfig.Get("docker", "account")
	passwd := loadConfig.Get("docker", "passwd")

	if account == "" || passwd == "" {
		return errors.New("docker账号不存在")
	}

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		logger.Errorf("PullImage NewClientWithOpts failed err:%s", err.Error())
		return err
	}

	authConfig := types.AuthConfig{
		Username: account,
		Password: passwd,
	}

	encodedJSON, err := json.Marshal(authConfig)
	if err != nil {
		logger.Errorf("PullImage Marshal failed err:%s", err.Error())
		return err
	}

	authStr := base64.URLEncoding.EncodeToString(encodedJSON)

	out, err := cli.ImagePull(ctx, imageName, types.ImagePullOptions{RegistryAuth: authStr})
	if err != nil {
		logger.Errorf("PullImage failed err:%s", err.Error())
		return err
	}

	defer out.Close()

	return nil
}
