package route

import (
	"github.com/gin-gonic/gin"
	"kay-gin/app/controller"
)

func groupDockerApi(r *gin.Engine) {
	dockerApiGroup := r.Group("/dockerapi")

	dockerApiGroup.GET("pullImage", controller.PullImage)
}
