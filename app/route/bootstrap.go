package route

import "github.com/gin-gonic/gin"

func LoadRouteGroup(r *gin.Engine) {
	groupKay(r)
	groupDockerApi(r)
}
