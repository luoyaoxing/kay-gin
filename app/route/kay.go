package route

import (
	"github.com/gin-gonic/gin"
	"kay-gin/app/loadConfig"
)

func groupKay(r *gin.Engine) {
	userGroup := r.Group("/kay")
	userGroup.GET("/getConfig", func(c *gin.Context) {
		mysqlDsn := loadConfig.Get("mysql", "xproject")
		c.String(200, mysqlDsn)
	})

	userGroup.GET("/user/:name", func(c *gin.Context) {
		name := c.Param("name")
		c.String(200, "Hello,"+name)
	})

	userGroup.GET("/users", func(c *gin.Context) {
		name := c.Query("name")
		role := c.DefaultQuery("role", "IT")
		c.JSON(200, gin.H{
			"name": name,
			"role": role,
		})
	})

	userGroup.POST("/form", func(c *gin.Context) {
		name := c.PostForm("name")
		passwd := c.DefaultPostForm("passwd", "636968")
		c.JSON(200, gin.H{
			"name":   name,
			"passwd": passwd,
		})
	})
}
