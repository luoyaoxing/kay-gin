package middleware

import (
	"github.com/gin-gonic/gin"
	"kay-gin/app/logger"
	"math"
	"time"
)

func RequestLatencyPercent() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()

		c.Next()

		latency := math.Ceil(float64(time.Since(t) / time.Second))
		reqPath := c.Request.Host + c.Request.URL.String()
		logger.Infof("reqLatencyPercent reqPath:%s latency:%d", reqPath, latency)
	}
}
