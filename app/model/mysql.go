package model

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"kay-gin/app/loadConfig"
	"kay-gin/app/logger"
	"xorm.io/core"
)

// 这里后续可以做一个连接池
func getMysqlImpl() (engine *xorm.Engine, err error) {
	engine, err = xorm.NewEngine("mysql", loadConfig.Get("mysql", "xproject"))
	if err != nil {
		logger.Errorf("xorm NewEngine err:%s", err.Error())
		return nil, err
	}

	tbMapper := core.NewPrefixMapper(core.SnakeMapper{}, "t_")
	fieldMapper := core.NewPrefixMapper(core.SnakeMapper{}, "f")

	engine.SetTableMapper(tbMapper)
	engine.SetColumnMapper(fieldMapper)

	return engine, nil
}
