package model

import (
	"github.com/go-redis/redis/v8"
	"kay-gin/app/loadConfig"
)

func getRedisImpl() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr: loadConfig.Get("redis", "addr"),
	})
}
